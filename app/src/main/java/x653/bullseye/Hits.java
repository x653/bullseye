package x653.bullseye;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

class Hits {
    private final int[] hits;

    Hits(){
        hits = new int[7];
    }

    int[] getHits(){return hits;}
    void reset(){
        for (int i=0;i<hits.length;i++) hits[i]=0;
    }

    int addDart(Dart dart){
        int index=index(dart.getValue());
        if (index>=0) hits[index]+=dart.getFactor();
        if (isClosed(dart.getValue())){
            return Math.min(hits[index]-3,dart.getFactor())*dart.getValue();
        }
        return 0;
    }
    int removeDart(Dart dart){
        int index=index(dart.getValue());
        int points=0;
        if (index>=0){
            if (isClosed(dart.getValue())) {
                points=Math.min(hits[index]-3,dart.getFactor())*dart.getValue();
            }
            hits[index]-=dart.getFactor();
        }
        return points;
    }

    boolean allClosed(){
        for (int hit : hits) if (hit < 3) return false;
        return true;
    }

    boolean isClosed(int field){
        int index=index(field);
        if (index==-1) return false;
        return (hits[index]>=3);
    }
    private int index(int field){
        if (field==25) return 6;
        if (field>=15 && field<=20) return 20-field;
        return -1;
    }
}
