package x653.bullseye;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import android.view.View;

import x653.bullseye.MainActivity;

interface Controller {
    void in(char c);
    boolean isX01();
    int getPlayer();
    String getPlayer(int i);
    void setPlayer(int i, String name);
    void drawStats(View view);
    void load(MainActivity main);
    void save(MainActivity main);
    int[] getLegs();
    void setLegs(int[] legs);
}

