package x653.bullseye;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

enum Factor {
    SINGLE, DOUBLE, TRIPLE;
    public String toString() {
        switch (this) {
            case SINGLE:
                return "";
            case DOUBLE:
                return "D";
            case TRIPLE:
                return "T";
        }
        return null;
    }

    public int value() {
        switch (this) {
            case SINGLE:
                return 1;
            case DOUBLE:
                return 2;
            case TRIPLE:
                return 3;
        }
        return 0;
    }
}