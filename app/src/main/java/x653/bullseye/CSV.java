package x653.bullseye;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import android.util.Log;

import java.util.ArrayList;

class CSV {

    static ArrayList<Darts> parseArrows(String string){
        ArrayList<Darts> arrows = new ArrayList<>();
        if (string!=null) {
            String[] lines = string.split("\n",-1);
            Log.d("bullseye length load",Integer.toString(lines.length));

            for (int i=0;i<lines.length-1;i++) {
                String line=lines[i];
                Darts darts = new Darts();
                Log.d("bullseye FileIO",line);
                String[] cols = line.split("-");
                for (String s:cols){
                    Factor factor=Factor.SINGLE;
                    if (s.startsWith("D")) {
                        factor=Factor.DOUBLE;
                        s=s.substring(1);
                    }
                    else if (s.startsWith("T")){
                        factor=Factor.TRIPLE;
                        s=s.substring(1);
                    }
                    else if (s.startsWith("BULL")){
                        s="25";
                    }
                    if (s.length()>0)
                        darts.addDart(new Dart(factor,Integer.valueOf(s)));
                }
                Log.d("bullseye FileIO",darts.toString());
                arrows.add(darts);
            }
        }
        return arrows;
    }

    static String toCSVString(ArrayList<Darts> arrows){
        Log.d("bullseye length save",Integer.toString(arrows.size()));
        StringBuilder stringBuilder=new StringBuilder();
        for (Darts arrow:arrows){
            stringBuilder.append(arrow.toString());
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
    static String toCSVString(PlayerX01 playerX01){
            return playerX01.toString();
    }
}