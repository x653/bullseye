package x653.bullseye;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import x653.bullseye.R;

class X01View {
    private final TextView[] legs = new TextView[3];
    private final EditText[] names = new EditText[3];
    private final TextView[] cards = new TextView[3];
    private final TextView[] scores = new TextView[3];
    private final TextView[] striche = new TextView[3];
    private final TextView[] legLabel = new TextView[2];
    private final View cardsView;
    private final ScrollView sv;
    private final View activita_main;


    X01View(AppCompatActivity main){
        sv=main.findViewById(R.id.sv1);
        activita_main=main.findViewById(R.id.activity_main);
        legs[0] = (TextView) main.findViewById(R.id.legs1);
        legs[1] = (TextView) main.findViewById(R.id.legs2);
        legs[2] = (TextView) main.findViewById(R.id.legs3);

        scores[0] = (TextView) main.findViewById(R.id.home);
        scores[1] = (TextView) main.findViewById(R.id.away);
        scores[2] = (TextView) main.findViewById(R.id.player3score);

        cardsView=main.findViewById(R.id.id_cards);
        cards[0] = (TextView) main.findViewById(R.id.homecard);
        cards[1] = (TextView) main.findViewById(R.id.awaycard);
        cards[2] = (TextView) main.findViewById(R.id.player3card);

        names[0] = (EditText) main.findViewById(R.id.player1);
        names[1] = (EditText) main.findViewById(R.id.player2);
        names[2] = (EditText) main.findViewById(R.id.player3);

        striche[0] = (TextView) main.findViewById(R.id.strich1);
        striche[1] = (TextView) main.findViewById(R.id.strich2);
        striche[2] = (TextView) main.findViewById(R.id.strich3);

        legLabel[0] = (TextView) main.findViewById(R.id.labelleg1);
        legLabel[1] = (TextView) main.findViewById(R.id.labelleg2);
    }

    X01View(AppCompatActivity main,int player){
        sv=main.findViewById(R.id.sv1);
        activita_main=main.findViewById(R.id.activity_main);
        legs[0] = (TextView) main.findViewById(R.id.legs1);
        legs[1] = (TextView) main.findViewById(R.id.legs2);
        legs[2] = (TextView) main.findViewById(R.id.legs3);

        scores[0] = (TextView) main.findViewById(R.id.home);
        scores[1] = (TextView) main.findViewById(R.id.away);
        scores[2] = (TextView) main.findViewById(R.id.player3score);

        cardsView=main.findViewById(R.id.id_cards);
        cards[0] = (TextView) main.findViewById(R.id.homecard);
        cards[1] = (TextView) main.findViewById(R.id.awaycard);
        cards[2] = (TextView) main.findViewById(R.id.player3card);

        names[0] = (EditText) main.findViewById(R.id.player1);
        names[1] = (EditText) main.findViewById(R.id.player2);
        names[2] = (EditText) main.findViewById(R.id.player3);

        striche[0] = (TextView) main.findViewById(R.id.strich1);
        striche[1] = (TextView) main.findViewById(R.id.strich2);
        striche[2] = (TextView) main.findViewById(R.id.strich3);

        legLabel[0] = (TextView) main.findViewById(R.id.labelleg1);
        legLabel[1] = (TextView) main.findViewById(R.id.labelleg2);
        setPlayer(player);
    }

    public void drawLegs(int player,int[] l){
        switch (player){
            case 1:
                legs[0].setText(String.format("%d",l[0]));
                break;
            case 2:
                legs[0].setText(String.format("%d:%d",l[0],l[1]));
                break;
            case 3:
                legs[0].setText(String.format("%d",l[0]));
                legs[1].setText(String.format("%d",l[1]));
                legs[2].setText(String.format("%d",l[2]));
                break;
        }
    }

    public CharSequence getPlayerName(int i){
        CharSequence s=names[i].getText();
        if (s.length() == 0) s = names[i].getHint();
        return s;
    }

    public void drawScores(int i, SpannableString s){
        scores[i].setText(s, TextView.BufferType.SPANNABLE);
    }

    public void drawCards(int i,SpannableString s){
        cards[i].setText(s,TextView.BufferType.SPANNABLE);
        if ((cards[0].getText().length()==0)&&
                (cards[1].getText().length()==0)&&
                (cards[2].getText().length()==0)) cardsView.setVisibility(View.GONE);
        else cardsView.setVisibility(View.VISIBLE);
    }

    public void setPlayer(int n){
        switch (n){
            case 1:
                legs[1].setVisibility(View.GONE);
                legs[2].setVisibility(View.GONE);
                names[1].setVisibility(View.GONE);
                names[2].setVisibility(View.GONE);
                names[0].setTextSize(24);
                names[1].setTextSize(24);
                names[2].setTextSize(24);
                names[0].setHint("HOME");
                names[1].setHint("AWAY");
                cards[2].setVisibility(View.GONE);
                scores[1].setGravity(Gravity.LEFT);
                scores[2].setVisibility(View.GONE);
                striche[0].setVisibility(View.GONE);
                striche[1].setVisibility(View.GONE);
                striche[2].setVisibility(View.GONE);
                legLabel[0].setVisibility(View.GONE);
                legLabel[1].setVisibility(View.GONE);
                break;
            case 2:
                legs[1].setVisibility(View.GONE);
                legs[2].setVisibility(View.GONE);
                names[1].setVisibility(View.VISIBLE);
                names[2].setVisibility(View.GONE);
                names[0].setTextSize(24);
                names[1].setTextSize(24);
                names[2].setTextSize(24);
                names[0].setHint("HOME");
                names[1].setHint("AWAY");
                cards[2].setVisibility(View.GONE);
                scores[1].setGravity(Gravity.CENTER_HORIZONTAL);
                scores[2].setVisibility(View.GONE);
                striche[0].setVisibility(View.GONE);
                striche[1].setVisibility(View.GONE);
                striche[2].setVisibility(View.GONE);
                legLabel[0].setVisibility(View.GONE);
                legLabel[1].setVisibility(View.GONE);
                break;
            case 3:
                legs[1].setVisibility(View.VISIBLE);
                legs[2].setVisibility(View.VISIBLE);
                names[1].setVisibility(View.VISIBLE);
                names[2].setVisibility(View.VISIBLE);
                names[0].setTextSize(22);
                names[1].setTextSize(22);
                names[2].setTextSize(22);
                names[0].setHint("Player1");
                names[1].setHint("Player2");
                cards[2].setVisibility(View.VISIBLE);
                scores[1].setGravity(Gravity.CENTER_HORIZONTAL);
                scores[2].setVisibility(View.VISIBLE);
                striche[0].setVisibility(View.VISIBLE);
                striche[1].setVisibility(View.VISIBLE);
                striche[2].setVisibility(View.VISIBLE);
                legLabel[0].setVisibility(View.VISIBLE);
                legLabel[1].setVisibility(View.VISIBLE);
                break;
        }
    }

    void setPlayerName(int i,String name){
        names[i].setText(name);
    }

    void focus(){
        sv.scrollTo(0, sv.getBottom());
        activita_main.requestFocus();
    }
}
