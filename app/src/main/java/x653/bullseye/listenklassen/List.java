package x653.bullseye.listenklassen;

/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

    public class List<ContentType> {

        private Node<ContentType> first;
        private Node<ContentType> last;
        private Node<ContentType> current;

        public List() {
            first = null;
            last = null;
            current = null;
        }

        public boolean isEmpty() {
            return (first == null);
        }

        public boolean hasAccess() {
            return (current != null);
        }

        public void next() {
            if (hasAccess()) {
                current = current.getNext();
            }
        }

        public void toFirst() {
            current = first;
        }

        public void toLast() {
            current = last;
        }

        public ContentType getObject() {
            if (hasAccess()) {
                return current.getContent();
            }
            return null;
        }

        public void setObject(ContentType pObject) {
            if (hasAccess() && pObject != null) {
                current.setContent(pObject);
            }
        }

        public void append(ContentType pObject) {
            if (pObject != null) {
                Node<ContentType> n = new Node<>(pObject);
                if (isEmpty()) {
                    first = n;
                } else {
                    last.setNext(n);
                }
                last = n;
            }
        }

        public void insert(ContentType pObject) {
            if (pObject!=null){
                if (isEmpty()) {
                    append(pObject);
                } else if (hasAccess()) {
                    Node<ContentType> p = current.getPrev();
                    Node<ContentType> n = new Node<>(pObject);
                    n.setNext(current);
                    if (p!=null) {
                        p.setNext(n);
                    } else {
                        first=n;
                    }
                }
            }

        }

        public void concat(List<ContentType> pList) {
            if (pList != null && !pList.isEmpty()) {
                if (isEmpty()) {
                    this.first = pList.first;
                } else {
                    this.last.setNext(pList.first);
                }
                this.last = pList.last;
                pList.first = null;
                pList.last = null;
                pList.current = null;
            }
        }

        public void remove() {
            if (hasAccess()) {
                Node<ContentType> n=current.getNext();
                Node<ContentType> p=getPrev();
                current=n;
                if (p!=null) {p.setNext(n);} else {first=n;}
                if (n==null) {last=p;}
            }
        }

        private Node<ContentType> getPrev(){
            if (isEmpty()) return null;
            if (!hasAccess()) return null;
            if (current==first) return null;
            Node<ContentType> p=first;
            while (p.getNext()!=current){
                p=p.getNext();
            }
            return p;
        }

        @Override
        public String toString(){
            StringBuilder s=new StringBuilder();
            toFirst();
            while (hasAccess()){
                s.append(getObject());
                next();
            }
            return s.toString();
        }

    }
