package x653.bullseye;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.StrikethroughSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import x653.bullseye.R;

class CricketView {
    private final TextView legs;
    private final EditText[] names = new EditText[2];
    private final TextView[] cards = new TextView[2];
    private final TextView[] scores = new TextView[2];
    private final View cardsView;
    private final ScrollView sv;
    private final View activita_main;
    private final ImageView[] imageViewHome=new ImageView[7];
    private final ImageView[] imageViewAway=new ImageView[7];
    private final Resources resources;


    CricketView(AppCompatActivity main){
        resources = main.getResources();
        imageViewHome[0]=main.findViewById(R.id.imageViewHome20);
        imageViewHome[1]=main.findViewById(R.id.imageViewHome19);
        imageViewHome[2]=main.findViewById(R.id.imageViewHome18);
        imageViewHome[3]=main.findViewById(R.id.imageViewHome17);
        imageViewHome[4]=main.findViewById(R.id.imageViewHome16);
        imageViewHome[5]=main.findViewById(R.id.imageViewHome15);
        imageViewHome[6]=main.findViewById(R.id.imageViewHomeBull);
        imageViewAway[0]=main.findViewById(R.id.imageViewAway20);
        imageViewAway[1]=main.findViewById(R.id.imageViewAway19);
        imageViewAway[2]=main.findViewById(R.id.imageViewAway18);
        imageViewAway[3]=main.findViewById(R.id.imageViewAway17);
        imageViewAway[4]=main.findViewById(R.id.imageViewAway16);
        imageViewAway[5]=main.findViewById(R.id.imageViewAway15);
        imageViewAway[6]=main.findViewById(R.id.imageViewAwayBull);
        //imageView.setImageResource(R.mipmap.check);
        sv=main.findViewById(R.id.sv1);
        activita_main=main.findViewById(R.id.cricket);
        legs = main.findViewById(R.id.legs1);

        scores[0] = main.findViewById(R.id.home);
        scores[1] = main.findViewById(R.id.away);

        cardsView = main.findViewById(R.id.id_cards);
        cards[0] = main.findViewById(R.id.homecard);
        cards[1] = main.findViewById(R.id.awaycard);

        names[0] =  main.findViewById(R.id.player1);
        names[1] =  main.findViewById(R.id.player2);
    }

    public void drawLegs(int[] l){
        legs.setText(String.format("%d:%d",l[0],l[1]));
    }

    public CharSequence getPlayerName(int i){
        CharSequence s=names[i].getText();
        if (s.length() == 0) s = names[i].getHint();
        return s;
    }

    public void drawHits(int player,int[] scores){
        ImageView[] images=imageViewHome;
        if (player==1) images=imageViewAway;
        for (int i=0;i<7;i++){
            switch (scores[i]){
                case 0:
                    images[i].setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    images[i].setImageResource(R.mipmap.check);
                    images[i].setVisibility(View.VISIBLE);
                    break;
                case 2:
                    images[i].setImageResource(R.mipmap.cross);
                    images[i].setVisibility(View.VISIBLE);
                    break;
                default:
                    images[i].setImageResource(R.mipmap.close);
                    images[i].setVisibility(View.VISIBLE);
                    break;
            }
        }
    }

    public void drawScores(int i, String s){
        scores[i].setText(durchstreichen(s), TextView.BufferType.SPANNABLE);
    }

    public void drawCards(boolean aktive,int i,String s){
        if (aktive){
            SpannableString spannableString=new SpannableString(s);
            spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), 0);
            cards[i].setText(spannableString,TextView.BufferType.SPANNABLE);
        } else {
            cards[i].setText(s, TextView.BufferType.SPANNABLE);
        }
    }

    void focus(){
        sv.scrollTo(0, sv.getBottom());
        activita_main.requestFocus();
    }

    void setPlayerName(int i,String name){
        names[i].setText(name);
    }

    private SpannableString durchstreichen(String sc){
        SpannableString s=new SpannableString(sc);
        int n=s.length()/9-1;
        for (int j=0;j<n;j++){
            if (s.charAt(j*9+4)!=' ') s.setSpan(new StrikethroughSpan(), j*9+4, j*9+8, 0);
            else if (s.charAt(j*9+5)!=' ') s.setSpan(new StrikethroughSpan(), j*9+5, j*9+8, 0);
            else if (s.charAt(j*9+6)!=' ') s.setSpan(new StrikethroughSpan(), j*9+6, j*9+8, 0);
            else if (s.charAt(j*9+7)!=' ') s.setSpan(new StrikethroughSpan(), j*9+7, j*9+8, 0);
        }
        return s;
    }

}
