package x653.bullseye;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ControllerCricket implements Controller {
    private Factor factor;
    private final CricketView cricketView;
    private Cricket cricket;
    private final MainActivity main;

    ControllerCricket(MainActivity main){
        this.main=main;
        cricketView = new CricketView(main);
        newGame();
    }

    @Override
    public void in(char c) {
        switch (c){
            case 'C':
                if (factor!= Factor.SINGLE) factor = Factor.SINGLE;
                else if (cricket.isEnd()) delete();
                else if (!cricket.getSet().isEmpty()) cricket.removeDart();
                else if (!cricket.isEmpty()) delete();
                else info();
                break;
            case 'T':
                if (!cricket.isEnd()&&!cricket.getSet().isFull()){
                    if (factor == Factor.TRIPLE) factor = Factor.SINGLE;
                    else factor = Factor.TRIPLE;
                } else info();
                break;
            case 'D':
                if (!cricket.isEnd()&&!cricket.getSet().isFull()){
                    if (factor == Factor.DOUBLE) factor = Factor.SINGLE;
                    else factor = Factor.DOUBLE;
                } else info();
                break;
            case 'O':
                if (cricket.isEnd()){
                    cricket.newLeg();
                    gameOn();
                }
                else {
                    cricket.nextPlayer();
                    factor= Factor.SINGLE;
                }
                break;
            case 'I':
                info();
                break;
            case 'B':
                if (!cricket.isEnd() && !cricket.getSet().isFull()){
                    if (factor==Factor.TRIPLE) main.toast("Triple Bull?\nMeine Oma!");
                    else {
                        cricket.addDart(new Dart(factor, 25));
                        factor = Factor.SINGLE;
                        if (cricket.isEnd()) {
                            gameShot();
                        }
                    }
                }else info();
                break;
            default:
                if (!cricket.isEnd() && !cricket.getSet().isFull()){
                    int v=c-'0'+10;
                    if (v==10) v=20;
                    cricket.addDart(new Dart(factor, v));
                    factor = Factor.SINGLE;
                    if (cricket.isEnd()) {
                        gameShot();
                    }
                }
                else info();
                break;
        }
        updateView();
    }
    void info(){
        if (cricket.isEmpty()) gameOn();
        else if (cricket.isEnd()) main.toast("Press RETURN to start new Leg");
        else if (cricket.getSet().isEmpty()) main.toast(String.format("%s to throw",cricketView.getPlayerName(cricket.getPlayer())));
        else if (cricket.getSet().isFull()) main.toast("Press RETURN to continue");
        else main.toast("Enter next dart");
    }

    private void updateView(){
        cricketView.drawLegs(cricket.getLegs());
        cricketView.drawScores(0,cricket.getPointsList(0));
        cricketView.drawScores(1,cricket.getPointsList(1));
        cricketView.drawHits(0,cricket.getHits(0).getHits());
        cricketView.drawHits(1,cricket.getHits(1).getHits());
        if (cricket.isEnd()) {
            cricketView.drawCards(true, cricket.getPlayer(), "");
            cricketView.drawCards(false, cricket.getOtherPlayer(), "");

        }else{
            String card = cricket.getSet().toString();
            if (!cricket.getSet().isEmpty() && factor != Factor.SINGLE) card = card + "-";
            card = card + factor.toString();
            while (card.length() < 11) card = card + "_";
            cricketView.drawCards(true, cricket.getPlayer(), card);
            cricketView.drawCards(false, cricket.getOtherPlayer(), "");
        }
    }

    public void newGame() {
        cricket = new Cricket();
        factor = Factor.SINGLE;
        updateView();
    }

    private void delete(){
        android.support.v7.app.AlertDialog alrt = new android.support.v7.app.AlertDialog.Builder(main).create();
        alrt.setTitle("Delete");
        alrt.setMessage("Clear last or all score?");
        alrt.setButton(AlertDialog.BUTTON_NEGATIVE, "All", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                newGame();
            }
        });
        alrt.setButton(AlertDialog.BUTTON_NEUTRAL, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alrt.setButton(AlertDialog.BUTTON_POSITIVE, "Last", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cricket.removeDart();
                updateView();
            }
        });
        alrt.show();
    }

    @Override
    public boolean isX01() {
        return false;
    }


    @Override
    public int getPlayer() {
        return 2;
    }

    public String getPlayer(int i) {
        return cricketView.getPlayerName(i).toString();
    }
    public void setPlayer(int i, String name){
        cricketView.setPlayerName(i,name);
    }

    @Override
    public void drawStats(View view) {

    }
    public void load(MainActivity main){
        cricket.loadDarts(CSV.parseArrows(FileIO.load(main)));
        updateView();
    }

    @Override
    public void save(MainActivity main) {
        FileIO.save(main,CSV.toCSVString(cricket.getDarts()));
    }

    @Override
    public int[] getLegs() {
        return cricket.getLegs();
    }

    public void setLegs(int[] legs){
        cricket.setLegs(legs);
        updateView();
    }

    private void gameShot(){
        main.toast(String.format("Game shot and the %s leg - %s!",legnr(),cricketView.getPlayerName(cricket.getPlayer())));
    }
    private String legnr(){
        int l = cricket.getLeg();
        String[] legs = main.getResources().getStringArray(R.array.legs);
        String legnr = String.format("%d.", l);
        if (l <= 10) legnr = legs[l - 1];
        return legnr;
    }
    private void gameOn(){
        Toast toast = Toast.makeText(main,
                String.format("%s leg - it's %s to throw first\nGame on!", legnr(), cricketView.getPlayerName(cricket.getPlayer())),
                Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) v.setGravity(Gravity.CENTER);
        toast.show();
    }

}