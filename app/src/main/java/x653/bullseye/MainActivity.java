package x653.bullseye;

/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import x653.bullseye.BuildConfig;
import x653.bullseye.Controller;
import x653.bullseye.ControllerCricket;
import x653.bullseye.ControllerX01;
import x653.bullseye.R;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Controller d;
    private Snackbar snackbar;

    private void SavePreferences(){
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (d.isX01()) {
            editor.putBoolean("x01",true);
            editor.putInt("player", d.getPlayer());
            editor.putInt("score", ((ControllerX01)d).getScore());
        }else {
            editor.putBoolean("x01",false);
        }
        d.save(this);
        int n = d.getPlayer();
        for (int i=0;i<n;i++){
            editor.putString("player"+i,d.getPlayer(i));
            editor.putInt("Leg"+i,d.getLegs()[i]);
        }
        editor.apply();   // I missed to save the data to preference here,
    }

    private void LoadPreferences() {
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        boolean isX01 = sharedPreferences.getBoolean("x01",true);
        if (isX01) {
            int score=sharedPreferences.getInt("score", 501);
            int player=sharedPreferences.getInt("player", 2);
            setX01(player,score);
        }
        else {
            setCricket();
        }
        d.load(this);
        int n = d.getPlayer();
        int[] legs=new int[n];
        for (int i=0;i<n;i++){
            String name=sharedPreferences.getString("player"+i,"");
            if (!name.equals("AWAY") && !name.equals("HOME") && !name.startsWith("Player"))
                d.setPlayer(i,name);
            legs[i]=sharedPreferences.getInt("Leg"+i,0);
        }
        d.setLegs(legs);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        LoadPreferences();
    }


    @Override
    public void onDestroy() {
        SavePreferences();
        super.onDestroy();
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(snackbar!=null && snackbar.isShown()) {
                finish();
                super.onBackPressed();
            } else {
                snackbar = Snackbar.make(this.findViewById(R.id.appBar),"Press BACK again to exit",Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        }
    }

    public void hint(View view){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        d.in('I');
    }

    public void sendMessage(View view) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        int taste = view.getId();
        switch (taste) {
            case R.id.button_clear:
                d.in('C');
                break;
            case R.id.button_OK:
                d.in('O');
                break;
            case R.id.button_stat:
                d.in('I');
                break;
            case R.id.button_add:
                d.in('+');
                break;
            default:
                CharSequence label=((Button) view).getText();
                if (label.length()==2) d.in(label.charAt(1));
                else d.in(label.charAt(0));
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case (R.id.nav_about):
                View view = LayoutInflater.from(this).inflate(R.layout.about, null);

                String versionName= BuildConfig.VERSION_NAME;
                int versionCode= BuildConfig.VERSION_CODE;
                if (versionName!=null)
                    ((TextView)view.findViewById(R.id.version))
                            .setText(String.format("%s (%d)",versionName,versionCode));

                AlertDialog alrt = new AlertDialog.Builder(this).setView(view).create();
                alrt.setTitle(R.string.about_title);
                alrt.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });
                alrt.show();
                break;
            case (R.id.menu_stat):
                View view_stat = LayoutInflater.from(this).inflate(R.layout.statistics, null);
                AlertDialog stat_alrt = new AlertDialog.Builder(this).setView(view_stat).create();
                d.drawStats(view_stat);

                stat_alrt.setTitle("Statistics");
                stat_alrt.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });
                stat_alrt.show();
                break;
            case (R.id.nav_help):
                View set = LayoutInflater.from(this).inflate(R.layout.help, null);

                AlertDialog alert = new AlertDialog.Builder(this).setView(set).create();
                alert.setTitle("Help");
                alert.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });
                alert.show();
                break;
            case (R.id.nav_settings):
                View setting = LayoutInflater.from(this).inflate(R.layout.settings, null);
                final RadioButton one=((RadioButton)setting.findViewById(R.id.radioButtonOne));
                final RadioButton two=((RadioButton)setting.findViewById(R.id.radioButtonTwo));
                final RadioButton three=((RadioButton)setting.findViewById(R.id.radioButtonThree));
                final RadioButton x01=((RadioButton)setting.findViewById(R.id.radioButtonX01));
                final RadioButton cricket=((RadioButton)setting.findViewById(R.id.radioButtonCricket));
                final RadioButton exp=((RadioButton)setting.findViewById(R.id.radioExperimental));
                final SeekBar seekBar=setting.findViewById(R.id.seekBar);

                cricket.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (cricket.isChecked()) {
                            seekBar.setEnabled(false);
                            one.setEnabled(false);
                            three.setEnabled(false);
                            two.setChecked(true);
                        }
                    }
                });
                x01.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (x01.isChecked()){
                            seekBar.setEnabled(true);
                            one.setEnabled(true);
                            three.setEnabled(true);
                        }
                    }
                });

                if (d.isX01()){
                    x01.setChecked(true);
                    ((SeekBar)setting.findViewById(R.id.seekBar)).setProgress((((ControllerX01)d).getScore()-1)/100-1);
                }
                else {
                    cricket.setChecked(true);
                    seekBar.setEnabled(false);
                    one.setEnabled(false);
                    three.setEnabled(false);
                    two.setChecked(true);
                    ((SeekBar)setting.findViewById(R.id.seekBar)).setProgress(2);
                }
                if (d.getPlayer()==2) two.setChecked(true);
                else if (d.getPlayer()==3) three.setChecked(true);
                else one.setChecked(true);
                final TextView points=((TextView)setting.findViewById(R.id.points));

                ((SeekBar)setting.findViewById(R.id.seekBar)).setOnSeekBarChangeListener(
                        new SeekBar.OnSeekBarChangeListener(){
                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                                // TODO Auto-generated method stub
                                points.setText(Integer.toString(100*progress+101));
                            }
                        }
                );


                AlertDialog alertset = new AlertDialog.Builder(this).setView(setting).create();
                alertset.setTitle("Settings");
                alertset.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (whichButton== AlertDialog.BUTTON_POSITIVE){
                                    if (cricket.isChecked()) setCricket();
                                    else if (x01.isChecked()){
                                        int score=Integer.parseInt(points.getText().toString());
                                        int player=2;
                                        if (two.isChecked()) player=2;
                                        else if (three.isChecked()) player=3;
                                        else player=1;
                                        setX01(player,score);
                                    } else if (exp.isChecked()){
                                        int score=Integer.parseInt(points.getText().toString());
                                        int player=2;
                                        if (two.isChecked()) player=2;
                                        else if (three.isChecked()) player=3;
                                        else player=1;
                                        setExperiment(player,score);
                                    }
                                }
                            }
                        });
                alertset.setButton(AlertDialog.BUTTON_NEUTRAL, "Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });

                alertset.show();
                break;

        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void setCricket() {
        View C = findViewById(R.id.activity_main);
        if (C!=null) {
            ViewGroup parent = (ViewGroup) C.getParent();
            int index = parent.indexOfChild(C);
            parent.removeView(C);
            C = getLayoutInflater().inflate(R.layout.cricket, parent, false);
            parent.addView(C, index);
        }
        d = new ControllerCricket(this);
    }

    private void setX01(int player,int score){
        View C = findViewById(R.id.cricket);
        if (C!=null) {
            ViewGroup parent = (ViewGroup) C.getParent();
            int index = parent.indexOfChild(C);
            parent.removeView(C);
            C = getLayoutInflater().inflate(R.layout.activity_main, parent, false);
            parent.addView(C, index);
        }
        d = new ControllerX01(this);
        ((ControllerX01) d).setPlayer(player);
        ((ControllerX01) d).setScore(score);
    }

    void setExperiment(int player,int score){
        View C = findViewById(R.id.cricket);
        if (C!=null) {
            ViewGroup parent = (ViewGroup) C.getParent();
            int index = parent.indexOfChild(C);
            parent.removeView(C);
            C = getLayoutInflater().inflate(R.layout.activity_main, parent, false);
            parent.addView(C, index);
        }
        d=new ControllerX(this,player,score);
    }
    void toast(String s){
        Toast toast = Toast.makeText(this, s, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) v.setGravity(Gravity.CENTER);
        toast.show();
    }

}