package x653.bullseye;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.content.res.ResourcesCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;

public class ControllerX implements DialogInterface.OnClickListener, Controller {
    private final X01 x01;
    private final String[][] checkout;
    private final MainActivity main;
    private final X01View x01View;
    private StringBuilder tokens;

    public ControllerX(MainActivity m, int player, int score) {
        main = m;
        x01View = new X01View(main, player);
        checkout = new String[3][];
        checkout[0] = main.getResources().getStringArray(R.array.checkout);
        checkout[1] = main.getResources().getStringArray(R.array.twodart);
        checkout[2] = main.getResources().getStringArray(R.array.onedart);
        x01 = new X01(player, score);
        init();
        updateView();
    }

    void init() {
        tokens = new StringBuilder();
    }

    public void in(char c) {
        switch (c){
            case 'I':
                info();
                break;
            case 'O':
                if (x01.isEnd()) newLeg();
                else enterScore();
                break;
            case 'T':
            case 'D':
                if(x01.isEnd()||isNew()) info();
                else if (getLast() == c) {
                    removeLast();
                } else if (isFac(getLast())) {
                    removeLast();
                    tokens.append(c);
                } else if (isNum(getLast()) && getCount() < 3) {
                    tokens.append("+");
                    tokens.append(c);
                } else if (getLast() == '+' || getLast() == 0) {
                    tokens.append(c);
                } else {
                    info();
//                    main.toast("Press Enter to continue");
                }
                break;
            case '+':
                if(x01.isEnd()||isNew()) info();
                else if (isNum(getLast()) && getCount() < 3) {
                    tokens.append(c);
                } else {
                    info();
                    //main.toast("Press Enter to continue");
                }
                break;
            case 'C':
                removeLast();
                break;
            default:
                if(x01.isEnd()) info();
                else {
                    if (isNew()) info();
                    if (isNum(c)) {
                        tokens.append(c);
                        if (value() < 0) {
                            removeLast();
                        }
                    }
                }
                break;
        }

        updateView();
    }

    private boolean isOut(int i) {
        if (i == 170) return true;
        if (i == 167) return true;
        if (i == 164) return true;
        if (i == 161) return true;
        if (i == 160) return true;
        if (i >= 159) return false;
        return true;
    }

    void info(){
        if (x01.isEmpty()&&tokens.length()==0) main.toast(String.format("%s leg - it's %s to throw first\nGame on!", legnr(),
                x01View.getPlayerName(x01.getPlayer())));
        else if (x01.isEnd())main.toast("Press Enter to star new Leg");
          else  if (isOut(x01.getPoints(x01.getPlayer())))
            main.toast(String.format("%s you require %d!", x01View.getPlayerName(x01.getPlayer()),
                    x01.getPoints(x01.getPlayer())));
        else if(tokens.length()==0)
            main.toast(String.format("%s to throw", x01View.getPlayerName(x01.getPlayer())));
        else main.toast("Press Enter to continue");
    }


    int getCount(){
        return tokens.toString().split("\\+",-1).length;
    }
    char getLast(){
        if (tokens.length()>0) return tokens.charAt(tokens.length()-1);
        return 0;
    }
    void removeLast(){
        if (tokens.length()>0) tokens.deleteCharAt(tokens.length()-1);
        else delete();
    }
    boolean isNum(char c){
        if ((c-'0'>=0) && (c-'0'<=9)) return true;
        return false;
    }
    boolean isFac(char c){
        if (c=='T') return true;
        if (c=='D') return true;
        return false;
    }

    int value(){
        int v=0;
        String plus="\\+";
        String[] darts=tokens.toString().split(plus,-1);
        for (int i=0;i<darts.length;i++){
            int d=value(darts[i]);
            if (d<0) return -1;
            v+=value(darts[i]);
        }
        return v;
    }
    int valueIncomplete(){
        int v=0;
        String plus="\\+";
        String[] darts=tokens.toString().split(plus,-1);
        for (int i=0;i<darts.length-1;i++){
            int d=value(darts[i]);
            if (d<0) return -1;
            v+=value(darts[i]);
        }
        return v;
    }

    int value(String dart){
        if (dart.length()==0) return 0;
        int f=1;
        char factor=dart.charAt(0);
        if (isFac(factor)){
            switch (factor){
                case 'T':
                    f=3;
                    break;
                case 'D':
                    f=2;
                    break;
            }
            dart=dart.substring(1);
        }
        int v=0;
        for (int i=0;i<dart.length();i++){
            if (!isNum(dart.charAt(i))) return -1;
            v=v*10+(dart.charAt(i)-'0');
        }
        switch (f){
            case 1:
                if (v<=180) return v;
            case 2:
                if (v==25) return 50;
                if (v<=20) return 2*v;
            case 3:
                if (v<=20) return 3*v;
        }
        main.toast(String.format("Invalid score %s",dart));
        return -1;
    }

    public boolean isX01(){
        return false;
    }

    private void enterScore(){
        if (!x01.addScore(value())) main.toast("No Score!");
        init();
        if (x01.isEnd()) {
            gameShot();
        }
        updateView();
    }

    private void delete(){
        android.support.v7.app.AlertDialog alrt = new android.support.v7.app.AlertDialog.Builder(main).create();
        alrt.setTitle("Delete");
        alrt.setMessage("Clear last or all score?");
        alrt.setButton(AlertDialog.BUTTON_NEGATIVE,"All", this);
        alrt.setButton(AlertDialog.BUTTON_NEUTRAL,"Cancel", this);
        alrt.setButton(AlertDialog.BUTTON_POSITIVE,"Last", this);
        alrt.show();
    }
    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE: // yes
                    init();
                    x01.removeScore();
                    updateView();
                break;
            case DialogInterface.BUTTON_NEGATIVE: // no
                x01.newGame();
                updateView();
                break;
            case DialogInterface.BUTTON_NEUTRAL: // neutral
                break;
            default:
                break;
        }
    }

    public int getPlayer(){
        return x01.player;
    }
    public int getScore(){
        return x01.score;
    }

    private void newLeg(){
        init();
        x01.newLeg();
        updateView();
        info();
    }

    private void gameShot(){
        main.toast(String.format("Game shot and the %s leg - %s!",legnr(),
                x01View.getPlayerName(x01.getPlayer())));
    }
    private String legnr(){
        int l=x01.getLeg();
        String[] legs = main.getResources().getStringArray(R.array.legs);
        String legnr = String.format("%d.", l);
        if (l <= 10) legnr = legs[l-1];
        return legnr;
    }
    boolean isNew(){
        return (x01.isEnd() || x01.isEmpty()&&tokens.length()==0);
    }

    private void updateView(){
        for (int i=0;i<x01.player;i++){
            if (i==x01.getPlayer() && !isNew()){
                if (tokens.length()==0){
                    String sc;
                    sc=String.format("%s___     ",x01.getPointsList(i));
                    SpannableString s=durchstreichen(sc);
                    x01View.drawScores(i,s);
                }else {
                    String sc;
                    sc=String.format("%s%3d %4d",
                            x01.getPointsList(i),
                            value(),
                            x01.getPoints(i)-value());
                    SpannableString s=durchstreichen(sc);
                    s.setSpan(new ForegroundColorSpan(ResourcesCompat.getColor(main.getResources(), R.color.chalk2, null)),
                            s.length() - 4, s.length(), 0);
                    s.setSpan(new UnderlineSpan(), s.length() - 9, s.length() - 5, 0);
                    x01View.drawScores(i,s);
                }
            }else{
                String sc=String.format("%s",x01.getPointsList(i));//plus Leerzeichen
                x01View.drawScores(i,durchstreichen(sc));
            }
            drawCards(i);
           // if (x01.getPlayer()==1) x01View.drawScores(
           //         1,new SpannableString(x01.getAveragesPlus()));
        }
        x01View.drawLegs(x01.player,x01.legs);
    }
    boolean isRechner(){
        return tokens.toString().contains("+")||tokens.toString().contains("T")||tokens.toString().contains("D");
    }

    private SpannableString durchstreichen(String sc){
        SpannableString s=new SpannableString(sc);
        int n=s.length()/9-1;
        for (int j=0;j<n;j++){
            if (s.charAt(j*9+4)!=' ') s.setSpan(new StrikethroughSpan(), j*9+4, j*9+8, 0);
            else if (s.charAt(j*9+5)!=' ') s.setSpan(new StrikethroughSpan(), j*9+5, j*9+8, 0);
            else if (s.charAt(j*9+6)!=' ') s.setSpan(new StrikethroughSpan(), j*9+6, j*9+8, 0);
            else if (s.charAt(j*9+7)!=' ') s.setSpan(new StrikethroughSpan(), j*9+7, j*9+8, 0);
        }
        return s;
    }

    public void drawStats(View view_stat){
        StringBuilder s=new StringBuilder();
        for (int i=1;i<=x01.getLeg();i++){
            s.append(String.format("leg %d\n",i));
        }
    }

    @Override
    public void load(MainActivity main) {
    }

    @Override
    public void save(MainActivity main) {
    }

    @Override
    public int[] getLegs() {
        return x01.legs;
    }

    @Override
    public void setLegs(int[] legs) {
        x01.setLegs(legs);
        updateView();
    }

    private void drawEingabe(){

    }

    private void drawCards(int player){
        String r="";
        String c=checkout(player);
        if (player==x01.getPlayer()){
            if (isRechner())
                r=String.format("%s_ = %d",tokens.toString(),value());
        }
        String all=r;
        if (c.length()!=0)
            all=String.format("%s\n%s",r,c);
        SpannableString s=new SpannableString(all);
        s.setSpan(new ForegroundColorSpan(ResourcesCompat.getColor(main.getResources(), R.color.chalk2, null)),
                0, r.length(), 0);
        x01View.drawCards(player,s);
    }

    private String checkout(int pl){
        int i=x01.getPoints(pl);
        if (pl==x01.getPlayer()){
            i=i-valueIncomplete();
            int dart=getCount();
            switch (dart){
                case 1:
                    if ((i>=2)&&(i<=170)) return checkout[0][i];
                    break;
                case 2:
                    if ((i>=2)&&(i<=110)) return checkout[1][i];
                    break;
                case 3:
                    if ((i>=2)&&(i<=50)) return checkout[2][i];
                    break;
            }
        }else{
            if ((i>=2)&&(i<=170)) return checkout[0][i];
        }
        return "";
    }

    public String getPlayer(int i){
        return x01View.getPlayerName(i).toString();
    }
    public void setPlayer(int i, String name){
        x01View.setPlayerName(i,name);
    }
}