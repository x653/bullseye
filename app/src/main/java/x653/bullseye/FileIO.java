package x653.bullseye;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

//  FileIO speichert und liest Dateien

class FileIO {
    private static final String filename ="data.txt";

    static String load(AppCompatActivity activity){
        FileInputStream fileInputStream;
        try {
            fileInputStream = activity.openFileInput(filename);
            int c;
            StringBuilder temp= new StringBuilder();
            while( (c = fileInputStream.read()) != -1){
                temp.append(Character.toString((char) c));
            }
            fileInputStream.close();
            return temp.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    static String load(AppCompatActivity activity,int i){
        FileInputStream fileInputStream;
        try {
            fileInputStream = activity.openFileInput("data"+i+".txt");
            int c;
            StringBuilder temp= new StringBuilder();
            while( (c = fileInputStream.read()) != -1){
                temp.append(Character.toString((char) c));
            }
            fileInputStream.close();
            return temp.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    static void save(AppCompatActivity activity, int i,String data){
        FileOutputStream outputStream;
        try {
            outputStream = activity.openFileOutput("data"+i+".txt", Context.MODE_PRIVATE);
            outputStream.write(data.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    static void save(AppCompatActivity activity, String data){
        FileOutputStream outputStream;
        try {
            outputStream = activity.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(data.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
