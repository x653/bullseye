package x653.bullseye;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.util.ArrayList;

class Cricket {
    private final Hits[] hits;
    private final ArrayList<Darts> darts;
    private final int[] legs;

    Cricket(){
        darts=new ArrayList<>();
        legs = new int[2];
        hits = new Hits[2];
        newLeg();
    }
    ArrayList<Darts> getDarts(){return darts;}

    int[] getLegs(){return  legs;}
    int getLeg(){
        if (isEnd()) return legs[0]+legs[1];
        return  legs[0]+legs[1]+1;
    }
    boolean isEmpty(){
        return darts.size()<=(getLeg()+1)%2+1 && getSet().isEmpty();
    }
    Darts getSet(){
        return darts.get(darts.size()-1);
    }
    int getPlayer(){
        return (darts.size()+1)%2;
    }
    int getOtherPlayer(){
        return (getPlayer()+1)%2;
    }

    void addDart(Dart dart){
        if (!getSet().isFull() && !isEnd()) {
            getSet().addDart(dart);
            int points = hits[getPlayer()].addDart(dart);
            if (!hits[getOtherPlayer()].isClosed(dart.getValue())){
                getSet().addPoints(points);
            }
            if (isEnd()) legs[getPlayer()]++;
        }
    }

    void removeDart(){
        if (!getSet().isEmpty()){
            Dart dart = getSet().removeDart();
            if (isEnd()){
                legs[getPlayer()]--;
            }
            int points = hits[getPlayer()].removeDart(dart);
            if (!hits[getOtherPlayer()].isClosed(dart.getValue())) getSet().removePoints(points);
        } else {
            if (darts.size()>1) darts.remove(darts.size()-1);
        }
    }

    void nextPlayer(){
        if (!isEnd()) {
            darts.add(new Darts());
        }
    }

    void newLeg(){
        hits[0] = new Hits();
        hits[1] = new Hits();
        darts.clear();
        darts.add(new Darts());
        if (getLeg()%2==0) darts.add(new Darts());
    }

    private int getPoints(int player){
        int points=0;
        for(int i=player;i<darts.size();i+=2){
            points+=darts.get(i).getPoints();
        }
        return points;
    }

    boolean isEnd(){
        return hits[getPlayer()].allClosed() && getPoints(getPlayer()) >= getPoints(getOtherPlayer());
    }
    Hits getHits(int player){
        return hits[player];
    }
    String getPointsList(int player){
        StringBuilder stringBuilder=new StringBuilder();
        int points=0;
        boolean first=true;
        for (int i=player%2;i<darts.size();i+=2){
            int point=darts.get(i).getPoints();
            if (point!=0) {
                if (first){
                    stringBuilder.append("   ");
                    first=false;
                }else {
                    stringBuilder.append(String.format("%3d",point));
                    //stringBuilder.append(" ");
                }
                points+=point;
                stringBuilder.append(String.format("%5d\n",points));
                //stringBuilder.append('\n');
            }
        }
        return stringBuilder.toString();
    }

    void loadDarts(ArrayList<Darts> darts){
        if (darts!=null) {
            for (Darts dartss : darts) {
                for (Dart dart : dartss.getDarts()) {
                    if (dart != null) addDart(dart);
                }
                if (darts.indexOf(dartss) != darts.size() - 1) nextPlayer();
            }
        }
    }
    void setLegs(int[] legs){
        for (int i=0;i<legs.length;i++){
            this.legs[i]=legs[i];
        }
    }

}