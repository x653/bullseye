package x653.bullseye;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

class Darts {
    private final Dart[] darts;
    private int position;
    private int points;
    Darts(){
        points=0;
        position=0;
        darts = new Dart[3];
    }
    int getPosition(){
        return position;
    }
    boolean isFull(){
        return (position==3);
    }
    boolean isEmpty(){
        return (position==0);
    }
    Dart[] getDarts(){return darts;}

    void addDart(Dart dart){
        if (!isFull()) darts[position]=dart;
        position++;
    }
    void addPoints(int points){
        this.points=this.points+points;
    }
    int getPoints(){return points;}
    void removePoints(int points){
        this.points=this.points-points;
    }

    Dart removeDart(){
        if (position>0){
            position--;
            Dart dart=darts[position];
            darts[position]=null;
            return dart;
        }
        return null;
    }

    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i=0;i<darts.length;i++){
            if (darts[i]!=null) stringBuilder.append(darts[i].toString());
            if (i+1<darts.length && darts[i+1]!=null) stringBuilder.append("-");
        }
        return stringBuilder.toString();
    }

}
