package x653.bullseye;
/**
 Bullseye is a scoreboard for darts.
 Copyright (C) 2017 Michael Schröder (mi.schroeder@gmx.de)

 This programm is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

import java.util.ArrayList;

public class X01 {
    final ArrayList<Integer> scores;
    final int score;
    final int[] legs;
    final int player;

    X01(int player, int score){
        scores = new ArrayList<>();
        this.score = score;
        this.player = player;
        legs = new int[player];
    }

    void newGame(){
        scores.clear();
        for (int i=0;i<player;i++)
            legs[i]=0;
    }

    int first(){
        return (getLeg()-1)%player;
    }
    int[] getLegs(){return  legs;}
    int getLeg(){
        int leg=0;
        for (int l:legs){
            leg+=l;
        }
        if (isEnd()) return leg;
        return  leg+1;
    }
    boolean isEmpty(){ return scores.size()<=first();}
    int getPlayer(){
        return scores.size()%player;
    }

    boolean addScore(int darts){
           if (darts<=getPoints(getPlayer())){
               scores.add(darts);
               if(isEnd()){
                   legs[(getPlayer()+player-1)%player]++;
               }
               return true;
           }
           scores.add(0);
           return false;
    }

    void removeScore(){
        if (scores.size()>first()) {
            if (isEnd()){
                legs[(getPlayer()+player-1)%player]--;
            }
            scores.remove(scores.size()-1);
        }
    }

    void newLeg(){
        if (isEnd()){
            scores.clear();
            for (int i=0;i<first();i++) scores.add(0);
        }
    }

    int getPoints(int player){
        int points=score;
        for(int i=player;i<scores.size();i+=this.player){
            points-=scores.get(i);
        }
        return points;
    }

    boolean isEnd(){
        for (int i=0;i<player;i++){
            if (getPoints(i)==0) return true;
        }
        return false;
    }

    String getPointsList(int player){
        StringBuilder stringBuilder=new StringBuilder();
        int points=score;
        stringBuilder.append(String.format("    %4d\n",score));
        for (int i=player%this.player;i<scores.size();i+=this.player){
                if(i>=first()) {
                    points -= scores.get(i);
                    stringBuilder.append(String.format("%3d %4d\n", scores.get(i), points));
                }
        }
        return stringBuilder.toString();
    }

    void setScores(ArrayList<Integer> darts) {
        for (Integer dartss : darts) {
            scores.add(dartss);
        }
    }
    ArrayList<Integer> getScores(){
        return scores;
    }
    void setLegs(int[] legs){
        for (int i=0;i<legs.length;i++){
            this.legs[i]=legs[i];
        }
    }

    double[] getAverage(){
        double[] averages = new double[player];
        int[] count=new int[player];
        for (int i=first();i<scores.size();i++){
            averages[i%player]+=scores.get(i);
            count[i%player]++;
        }
        for (int i=0;i<player;i++){
            averages[i]=averages[i]/count[i];
        }
        return averages;
    }

}
