# Bullseye

Android app scoreboard for dart X01 and cricket

* scores x01 or cricket
* one, two or three player
* 301, 501, etc.
* dart calculator
* checkouts
* averages
* stylish design
 
[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">](https://f-droid.org/packages/x653.bullseye)
